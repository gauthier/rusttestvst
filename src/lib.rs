pub mod logging;

/*
#[macro_use]
extern crate vst;
extern crate chrono;
extern crate fern;

use vst::buffer::AudioBuffer;
use vst::api::{Events,Supported};
use vst::plugin::{Info, Plugin, CanDo};
use vst::event::Event;

pub mod lib {
    pub fn setup_logger() -> Result<(), fern::InitError> {
        fern::Dispatch::new()
            .format(|out, message, record| {
                out.finish(format_args!(
                    "{}[{}][{}] {}",
                    chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                    record.target(),
                    record.level(),
                    message
                ))
            })
            .level(log::LevelFilter::Debug)
            .chain(fern::log_file(r#"C:\dev\src\gitlab.com\gauthier\rusttestvst\waldorfmwxtvstoutput.log"#)?)
            .apply()?;
        Ok(())
    }
}*/
/*
#[derive(Default)]
struct BasicPlugin {
   

}

//#[derive(Default)]
//struct WaldorfMicrowaveVst;

impl BasicPlugin {

    fn process_midi_event(&mut self, event: vst::event::MidiEvent) {
        println!("{}{}{}", event.data[0], event.data[1], event.data[2]);
    }

    fn process_sysex_event(&mut self, event: vst::event::SysExEvent) {
        println!("{:?}", event.payload);
    }
}

impl Plugin for BasicPlugin {

    /// Process an incoming midi event.
    ///
    /// The midi data is split up like so:
    ///
    /// `data[0]`: Contains the status and the channel. Source: [source]
    /// `data[1]`: Contains the supplemental data for the message - so, if this was a NoteOn then
    ///            this would contain the note.
    /// `data[2]`: Further supplemental data. Would be velocity in the case of a NoteOn message.
    ///
    /// [source]: http://www.midimountain.com/midi/midi_status.htm    
    fn get_info(&self) -> Info {
        Info {
            name: "BasicPlugin".to_string(),
            vendor: "Gauthier".to_string(),
            unique_id: 11357, // Used by hosts to differentiate between plugins.
            category: vst::plugin::Category::Synth,
            inputs: 0,
            outputs: 0,
            parameters: 0,
            initial_delay: 0,

            ..Default::default()
        }
    }

    fn can_do(&self, can_do: CanDo) -> Supported {
        match can_do {
            CanDo::ReceiveMidiEvent  => Supported::Yes,
            CanDo::ReceiveSysExEvent => Supported::Yes,
            _                        => Supported::Maybe,
        }
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {}

    fn process_events(&mut self, events: &Events) {
        for e in events.events() {
            match e {
                Event::Midi(ev) => self.process_midi_event(ev),
                Event::SysEx(ev) => self.process_sysex_event(ev),
                // More events can be handled here.
                _ => (),
            }
        }
    }    
}

plugin_main!(BasicPlugin);*/