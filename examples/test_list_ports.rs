
extern crate midir;
//extern crate rusttestvst;
use std::io::{stdin, stdout, Write};
use std::error::Error;

use midir::{MidiInput, MidiOutput, Ignore};

fn main() {
    //let _ = ::logging::setup_logger();
    match run() {
        Ok(_) => (),
        Err(err) => println!("Error: {}", err.description())
    }
}

fn get_output_port_by_name(name: String) -> Option<midir::MidiOutputConnection> {
  match midir::MidiOutput::new("midir test output") {
    Err(_) => None
    , Ok(midi_out) => 
      {
        let mut port_id = None;
        for i in 0 .. midi_out.port_count() {
          let portname = midi_out.port_name(i);
          match portname {
            Ok(portname) =>
              if portname == name {
                port_id = Some(i);
                break;
              }
            , Err(_) => ()
          }
        }
        match port_id {
          Some(port_id) => 
            match midi_out.connect(port_id, "output") {
              Ok(midi_port) => Some(midi_port)
              , Err(_) => None
            }
          , None => None
        }
      }
  }
}

fn get_input_port_by_name<F,T>(name: String, callback: F, data: T) -> Option<midir::MidiInputConnection<T>>
where
   T : std::marker::Send
 , F : FnMut(u64, &[u8], &mut T) + Send + 'static,
 {
  match midir::MidiInput::new("midir test input") {
    Err(_) => None
    , Ok(midi_in) => 
      {
        let mut port_id = None;
        for i in 0 .. midi_in.port_count() {
          let portname = midi_in.port_name(i);
          match portname {
            Ok(portname) =>
              if portname == name {
                port_id = Some(i);
                break;
              }
            , Err(_) => ()
          }
        }
        match port_id {
          Some(port_id) => 
            match midi_in.connect(port_id, "input", callback, data) {
              Ok(midi_port) => Some(midi_port)
              , Err(_) => None
            }
          , None => None
        }
      }
  }
}


fn run() -> Result<(), Box<Error>> {
    let mut midi_in = MidiInput::new("midir test input")?;
    midi_in.ignore(Ignore::None);
    let midi_out = MidiOutput::new("midir test output")?;

    let mut input = String::new();

    loop {
        println!("Available input ports:");
        for i in 0..midi_in.port_count() {
            println!("{}: {}", i, midi_in.port_name(i)?);
        }
        
        println!("\nAvailable output ports:");
        for i in 0..midi_out.port_count() {
            println!("{}: {}", i, midi_out.port_name(i)?);
        }

        // run in endless loop if "--loop" parameter is specified
        match ::std::env::args().nth(1) {
            Some(ref arg) if arg == "--loop" => {}
            _ => break
        }
        print!("\nPress <enter> to retry ...");
        stdout().flush()?;
        input.clear();
        stdin().read_line(&mut input)?;
        println!("\n");
    }
    let portname = "microwavext";
    let _midiout = get_output_port_by_name(portname.to_string());
    let callback =  move |stamp: u64 , message: &[u8], _: &mut ()|  {
      println!("stamp: {} message: {:?}", stamp, message);
    };
    let data = ();
    let _midiin = get_input_port_by_name(portname.to_string(), callback, data);


    //println!("output: {:?}", midiout);
    //println!("input: {:?}", midiin);

    let mut input = String::new();

    stdin().read_line(&mut input)?;
    Ok(())
}